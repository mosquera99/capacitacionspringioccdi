package com.co.hosy.IOC;

public class DirectorEmpleado implements Empleados {

	// Creacion variable interfaz
	private CreacionInformes informes;

	public DirectorEmpleado(CreacionInformes informes) {
		this.informes = informes;
	}

	public String getTareas() {
		return "Gestiono la plantilla de la empresa";
	}

	public String getInforme() {
		return "Informe creado por el director: " + informes.GetInfome();
	}

}
