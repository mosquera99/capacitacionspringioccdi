package com.co.hosy.IOC;

public class SecretarioEmpleado implements Empleados {

	private CreacionInformes informes;
	private String email;
	private String nombreEmpresa;

	public String getTareas() {
		return "Gestionar el horario de los jefes";
	}

	public String getInforme() {
		return "Informe creado por el puto secretario. " + informes.GetInfome();
	}

	public void setInformes(CreacionInformes informes) {
		this.informes = informes;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

}
