package com.co.hosy.IOC;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoEmpleados {

	public static void main(String[] args) {
//		Empleados jf = new DirectorEmpleado();
//		System.out.println(jf.getTareas());

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		// Empleados juan = context.getBean("miEmpleado", Empleados.class);
		// System.out.println(juan.getTareas());
		// System.out.println(juan.getInforme());
		SecretarioEmpleado maria = context.getBean("miSecretario", SecretarioEmpleado.class);
		System.out.println("Results " + maria.getNombreEmpresa());
		System.out.println("Results " + maria.getEmail());
		System.out.println(maria.getInforme());
		context.close();
	}

}
