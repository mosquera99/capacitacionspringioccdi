package com.co.hosy.IOC;

public class JefeEmpleado implements Empleados {

	private CreacionInformes informes;

	public JefeEmpleado(CreacionInformes informes) {
		this.informes = informes;
	}

	public String getTareas() {
		return "Gestiono las cuestiones de mis empleados a la secci�n";
	}

	public String getInforme() {
		return "Informe presentado por el jefe con arreglos " + informes.GetInfome();
	}

}
